import discord
from discord.ext import commands
import time

with open('data.txt', 'r') as f: # 'r' is a reading mode
    text = f.read()

class Moderation(commands.Cog):
    def __init__(self, client):
        self.client = client
        self.__last_member = None

    @commands.Cog.listener()
    async def on_member_join(self, member):
        channel = member.guild.system_channel
        if channel is not None:
            await channel.send('Welcome {0.mention}.'.format(member))

    @commands.command()
    async def config(self, ctx, configaction, configdata):
        if configaction == 'auditchannel':
            if configdata ==  None:
                ctx.send('You Did Not Supply A Channel Id, Please Rerun The Command And Supply It With A Valid Channel ID.')   

            else:
                datafile = open('data.txt', 'w')
                datafile.write(configdata)
                embedVar = discord.Embed(title="Audit Log Channel", description=f"Setting the Audit Log Channel", color=0x059fff)
                embedVar.add_field(name="Audit Log Channel", value=f'<#{configdata}>', inline=True)
                await ctx.send(embed=embedVar)

    
    
    @commands.command()
    async def unban(self, ctx, *, member):
        banned_users = await ctx.guild.bans()
        member_name, member_discriminator = member.split('#')

        datafile = open('data.txt', 'r')
        AuditChannel = datafile.read()
        for ban_entry in banned_users:
            user = ban_entry.user

            if (user.name, user.discriminator) == (member_name, member_discriminator):
                await ctx.guild.unban(user)
                await ctx.send (f'Unbanned {user.mention}')
                print(f'Unbanned {user.mention}')
                await channel.send('Test')
                channel = self.client.get_channel(AuditChannel)
                embedVar = discord.Embed(title="Audit Log", description=f"Logging a unban.", color=0x059fff)
                embedVar.add_field(name="Unbanned User:", value=user.mention, inline=True)
                await channel.send(embed=embedVar)
                return
            else:
                await ctx.send ('User does not exist/is not banned')

    @commands.command()
    async def ban(self, ctx, member : discord.Member, *, reason=None):
        datafile = open('data.txt', 'r')
        AuditChannel = datafile.read()
        await member.ban(reason=reason)
        await ctx.send(f'Banned {member.mention}')
        channel = self.client.get_channel(AuditChannel)
        await channel.send(f'{member.mention} Was Banned!')
    @commands.command()
    async def kick(self, ctx, member : discord.Member, *, reason=None):
        await member.kick(reason=reason)

def setup(client):
    client.add_cog(Moderation(client))

